**Work in progess, script may not be functional.**

# xix

This script controls the packages installed with xbps, the groups associated with a user, the runit services and additional system configurations with a declarative approach. All mentioned configurations are set via a readable markdown file.
One or even both of the 'x's in the name 'xix' stand for 'XBPS'. The 'ix' is also supposed to remind of "nix", a declarative package manager. Then the 'i' surely stands for installation. Please make your own sense of this.

## Description


